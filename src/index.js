import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';

const Grid = () => {
  const grid = Array(27).fill(null);
  const numbers = Array.from({ length: 15 }, () => Math.floor(Math.random() * 90) + 1);

  let filledCount = 0;
  while (filledCount < 15) {
    const cellIndex = Math.floor(Math.random() * grid.length);
    if (grid[cellIndex] === null) {
      grid[cellIndex] = numbers[filledCount];
      filledCount++;
    }
  }

  return (
    <div className="grid">
      {grid.map((cell, index) => (
        <div key={index} className="cell">{cell}</div>
      ))}
    </div>
  );
};

ReactDOM.createRoot(document.getElementById("root")).render(<Grid />);
